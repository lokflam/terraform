local base = {
  "include": [".gitlab/ci/terraform.gitlab-ci.yml"],
};

local environment(name) = {
  [std.format('init:%s', name)]: {
    "environment": name,
    "extends": ".terraform-init"
  },
  [std.format('validate:%s', name)]: {
    "environment": name,
    "needs": [
      std.format('init:%s', name)
    ],
    "dependencies": [
      std.format('init:%s', name)
    ],
    "extends": ".terraform-validate"
  },
  [std.format('plan:%s', name)]: {
    "environment": name,
    "needs": [
      std.format('init:%s', name),
      std.format('validate:%s', name)
    ],
    "dependencies": [
      std.format('init:%s', name)
    ],
    "extends": ".terraform-plan"
  },
  [std.format('apply:%s', name)]: {
    "environment": name,
    "needs": [
      std.format('init:%s', name),
      std.format('plan:%s', name)
    ],
    "dependencies": [
      std.format('init:%s', name),
      std.format('plan:%s', name)
    ],
    "extends": ".terraform-apply"
  }
};

local script(environments) = std.foldl(function(x, y) (x + y), std.map(environment, environments), base);

function(environments) {
  "environments.gitlab-ci.yml": std.manifestYamlDoc(script(environments), true)
}
