terraform {
  backend "http" {
  }

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.83.0"
    }
  }
}

provider "google" {
  project = "lokllf-poc-10-1"
  region  = "asia-east1"
  zone    = "asia-east1-b"
}

resource "google_project_service" "services" {
  for_each = toset([
    "cloudresourcemanager.googleapis.com",
    "iam.googleapis.com",
    "compute.googleapis.com",
  ])

  service            = each.key
  disable_on_destroy = true
}
