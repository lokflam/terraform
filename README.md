# Terraform Monorepo

## Structure

### Environments

- Each environment has its own directory in `environments/`
- Each environment has a `.terraform-version` file for `tfenv` tool to install appropriate Terraform version
- Gitlab environments are automatically created and can be checked in `Deployments -> Environments`

### States

- States are stored in Gitlab using **HTTP backend**, an empty `backend "http"` block should be included in Terraform file
- State file and `terraform init` command can be checked in `Infrastructure -> Terraform`

### Pipeline

#### Setup

1. Create a Gitlab access token with `api` scope and set the token as `GITLAB_ACCESS_TOKEN` CI/CD variable
1. Create cloud credentials and store them in CI/CD variable
    - E.g. create `GOOGLE_CREDENTIALS` file type variable containing the key file content
1. Check container image [ci-build-image](https://gitlab.com/lokflam/ci-build-image) exists, which contains the tools required in the pipeline

#### Flow

1. List all directories in `environments/`, each will create a environment
1. Generate gitlab-ci script using `jsonnet`, using the listed environment names
1. Trigger the newly generated script as child pipeline
1. For each environment, if related files are changed, trigger jobs:
    1. Execute `terraform init` to initiate Gitlab state
    1. Execute `terraform validate`
    1. Execute `terraform plan` as save as artifact
        - If the committed branch is not `main`, developer can raise a merge request. Plan actions will be shown in the request
    1. Execute `terraform apply`
        - Can only be manually triggered in default (`main`) branch

### New environment setup

1. Create a new directory in `environments/`
1. Create `.terraform-version` file in the directory, containing just the Terraform version, e.g. `1.0.7`
1. Create a Terraform file with the minimum content as follow:

    ```tf
    terraform {
      backend "http" {
      }
    }
    ```

1. Push the files to a branch and trigger that pipeline
1. New environment and state in Gitlab will be created
1. Local setup
    1. Copy the `terraform init` command in `Environments` page and replace the `GITLAB_ACCESS_TOKEN` as your own token
    1. Run `terraform init` in local for development
